import React, { Component } from 'react';
import TweetBox from "./frontEnd/component/TweetBox";
import TweetList from "./frontEnd/component/TweetList";
import testdata from "./api/sampledata";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {tweetsList: testdata.items};
  }
  addTweet(tweetToAdd) {
    let newTweetsList = this.state.tweetsList;
    newTweetsList.unshift({ name: 'me', post: tweetToAdd });

    this.setState({ tweetsList: newTweetsList });
  }
  render() {
    return (
      <div className="container">
        <TweetBox sendTweet={this.addTweet.bind(this)}/>
        <TweetList tweets={this.state.tweetsList} />
      </div>
    );
  }
}

export default App;
