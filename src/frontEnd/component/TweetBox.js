import React from 'react';

export default class TweetBox extends React.Component {
  sendTweet(event) {
    event.preventDefault();
    this.props.sendTweet(this.refs.tweetTextArea.value);
    this.refs.tweetTextArea.value = '';
  }

  render() {
    return (
      <div className="row">
        <form onSubmit={this.sendTweet.bind(this)}>
          <div className="input-field">
            <textarea ref="tweetTextArea" className="materialize-textarea" />
            <label>update</label>
            <button class="mui-btn mui-btn--raised mui-btn--primary" type="submit" className="btn right">Post Update</button>
          </div>
        </form>
      </div>
    )
  }
}